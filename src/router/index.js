import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  { path: '*', redirect: { name: 'selectRoom' } },
  {
    path: '/chat',
    component: () =>
      import(/* webpackChunkName: "chat" */ '../views/chat/index.vue'),
    children: [
      {
        path: '',
        redirect: { name: 'selectRoom' }
      },
      {
        path: 'selectroom',
        name: 'selectRoom',
        component: () =>
          import(
            /* webpackChunkName: "selectRoom" */ '../views/chat/selectRoom.vue'
          )
      },
      {
        path: 'chatroom',
        name: 'chatRoom',
        component: () =>
          import(
            /* webpackChunkName: "chatRoom" */ '../views/chat/chatRoom.vue'
          )
      }
    ]
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
