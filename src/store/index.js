import Vue from 'vue'
import Vuex from 'vuex'
import socketIO from '../../node_modules/socket.io-client/dist/socket.io.js'
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    io: socketIO('http://localhost:3000', { forceNew: false }),
    io_payload: { room: '', name: '', msg: '', id: '' }
  },
  mutations: {
    io_edit_payload (state, data) {
      state.io = data
    }
  },
  actions: {
    io_update (context, data) {
      context.commit('io_edit_payload', data)
    }
  },
  modules: {},
  getters: {
    socket: state => state.io
  }
})
