import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import widgedtext from '@/components/widget_text.vue'
import widgedimage from '@/components/widget_image.vue'

Vue.config.productionTip = false
Vue.component('widget-text', widgedtext)
Vue.component('widget-image', widgedimage)
new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
